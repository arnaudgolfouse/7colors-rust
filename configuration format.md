A configuration file may be given to the game. Any line beginning with `#` will be ignored. It may contain the following lines:

- `P1_IA = <INT>` / `P2_IA = <INT>`

  Set the IA level for player 1/2. `0` means a human player.

- `P1_color = <COLOR>` / `P2_color = <COLOR>`

  Set the color for player 1/2. Defaults to `red` and `blue`. Valid colors are:

  - `black`
  - `red`
  - `green`
  - `yellow`
  - `blue`
  - `magenta`
  - `cyan`
  - `white`
  - `rgb { <INT>, <INT>, <INT> }`

- `board_size = <INT>`

  Size of the board. Defaults to `30`.

- `nb_colors = <INT>`

  Numbers of colors/letters that can appear on the board. Defaults to `30`.

  The minimum is `2`, and the maximum `26`.

- `tournament_round = <INT>`

  Numbers of rounds of game played. Should be used only when both players are IAs.

  Setting this to `0` means playing a regular game.
