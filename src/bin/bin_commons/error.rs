pub struct Error(pub Box<dyn ::core::fmt::Display>);
impl ::core::fmt::Debug for Error {
    fn fmt(&self, f: &mut ::core::fmt::Formatter<'_>) -> ::core::fmt::Result {
        write!(f, "{}", self.0)
    }
}
impl<E: ::core::fmt::Display + 'static> From<E> for Error {
    fn from(value: E) -> Self {
        Self(Box::new(value))
    }
}
