use ::std::{
    io::{Read as _, Write as _},
    net::TcpStream,
};

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum MessageKind {
    HelloFromServer = 200,
    HelloFromClient,
    NbColors,
    BoardSize,
    BoardInitialState,
    YouArePlayerNo,
    PlayMove,
    GameFinished,
    Ok = 255,
}

pub struct Communication {
    pub stream: TcpStream,
    pub buffer: Vec<u8>,
}

pub enum CommunicationError {
    Io(::std::io::Error),
    NoOk,
    Nothing,
    WrongKind { expected: u8, got: u8 },
}

impl From<::std::io::Error> for CommunicationError {
    fn from(value: ::std::io::Error) -> Self {
        Self::Io(value)
    }
}
impl ::core::fmt::Display for CommunicationError {
    fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
        match self {
            CommunicationError::Io(err) => ::core::fmt::Display::fmt(err, f),
            CommunicationError::NoOk => {
                f.write_str("the other side did not respond with a 'OK' message")
            }
            CommunicationError::Nothing => f.write_str("the other side sent nothing"),
            CommunicationError::WrongKind { expected, got } => write!(
                f,
                "the other side sent a {got:?} message instead of {expected:?}"
            ),
        }
    }
}

impl Communication {
    pub fn send_and_wait_ok(
        &mut self,
        kind: MessageKind,
        message: &[u8],
    ) -> Result<(), CommunicationError> {
        ::colors7::trace!("sending message {:?} with content {:?}", kind, message);
        self.stream.write_all(&[kind as u8])?;
        self.stream.write_all(message)?;

        ::colors7::trace!("message sent, receiving OK");
        loop {
            match self.stream.read(&mut self.buffer[0..1]) {
                Ok(n) => {
                    if n != 1 || self.buffer[0] != MessageKind::Ok as u8 {
                        return Err(CommunicationError::NoOk);
                    }
                    break Ok(());
                }
                Err(err) => match err.kind() {
                    ::std::io::ErrorKind::Interrupted => continue,
                    _ => return Err(err.into()),
                },
            }
        }
    }

    pub fn receive_and_send_ok(
        &mut self,
        kind: MessageKind,
        size: usize,
    ) -> Result<&mut [u8], CommunicationError> {
        ::colors7::trace!("receiving message {:?} of size {}", kind, size);
        loop {
            match self.stream.read(&mut self.buffer[..1]) {
                Ok(n) => {
                    if n == 0 {
                        return Err(CommunicationError::Nothing);
                    }
                    if self.buffer[0] != kind as u8 {
                        return Err(CommunicationError::WrongKind {
                            expected: kind as u8,
                            got: self.buffer[0],
                        });
                    }
                    break;
                }
                Err(err) => match err.kind() {
                    ::std::io::ErrorKind::Interrupted => continue,
                    _ => return Err(err.into()),
                },
            }
        }
        let mut start = 0;
        while size - start != 0 {
            match self.stream.read(&mut self.buffer[start..size]) {
                Ok(n) => {
                    if n == size - start {
                        break;
                    }
                    start += n;
                }
                Err(err) => match err.kind() {
                    ::std::io::ErrorKind::Interrupted => continue,
                    _ => return Err(err.into()),
                },
            }
        }
        ::colors7::trace!("message received: {:?}, sending OK", &self.buffer[..size]);
        self.stream.write_all(&[MessageKind::Ok as u8])?;
        Ok(&mut self.buffer[..size])
    }

    #[allow(dead_code)]
    pub fn receive_raw(&mut self, size: usize) -> Result<&mut [u8], CommunicationError> {
        ::colors7::trace!("receiving raw message of size {}", size);
        let mut start = 0;
        while size - start != 0 {
            match self.stream.read(&mut self.buffer[start..size]) {
                Ok(n) => {
                    if n == size - start {
                        break;
                    }
                    start += n;
                }
                Err(err) => match err.kind() {
                    ::std::io::ErrorKind::Interrupted => continue,
                    _ => return Err(err.into()),
                },
            }
        }
        ::colors7::trace!("message received, sending OK");
        self.stream.write_all(&[MessageKind::Ok as u8])?;
        Ok(&mut self.buffer[..size])
    }
}
