use ::colors7::{
    board::{Board, PlayerId},
    Color,
};

pub fn choose_move(board: &Board, current_player: PlayerId) -> u8 {
    let color = board.player_color(current_player);
    match board.player_kind(current_player) {
        ::colors7::PlayerKind::Human => {
            println!(
                "{}(P{}){} Choose a letter :",
                color.color_code_foreground(),
                match current_player {
                    PlayerId::Player1 => 1,
                    PlayerId::Player2 => 2,
                },
                Color::color_code_reset()
            );
            let mut input = String::new();
            loop {
                input.clear();
                ::std::io::stdin().read_line(&mut input).unwrap();
                let input = input.trim();
                if input.len() != 1 {
                    println!("Only one letter may be given as input!");
                } else {
                    let letter = input.as_bytes()[0];
                    if letter < b'A' || letter >= b'A' + board.config.nb_colors {
                        println!("Invalid move!");
                    } else {
                        break letter;
                    }
                }
            }
        }
        ::colors7::PlayerKind::Ia(ia) => board.choose_ia_move(current_player, ia),
    }
}
