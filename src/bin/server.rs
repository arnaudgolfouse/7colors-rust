#[path = "bin_commons/choose_move.rs"]
mod choose_move;
#[path = "bin_commons/communication.rs"]
mod communication;
#[path = "bin_commons/error.rs"]
mod error;

use self::{
    communication::{Communication, MessageKind},
    error::Error,
};
use ::colors7::{
    board::{Board, GameState, PlayerId},
    config::Config,
};
use ::std::{
    ffi::OsString,
    net::{Ipv4Addr, SocketAddr, SocketAddrV4, TcpListener},
    path::PathBuf,
};

#[allow(dead_code)]
fn main() {
    ::colors7::log::init_env_logger();
    let args = ::std::env::args_os().skip(1).collect();
    let (server, config_path) = match open_connection(args) {
        Ok(res) => res,
        Err(err) => {
            ::colors7::error!("{}", err.0);
            return;
        }
    };
    if let Err(err) = launch_server(server, config_path) {
        ::colors7::error!("{}", err.0);
    }
}

/// Initialize the server with the given args.
///
/// On success, returns the socket on which the server was bound, and the path of the
/// configuration file.
pub fn open_connection(args: Vec<OsString>) -> Result<(TcpListener, PathBuf), Error> {
    let (config_path, port) = parse_args(args)?;
    let socket_address = SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::UNSPECIFIED, port));
    let server = TcpListener::bind(socket_address)?;
    Ok((server, config_path))
}

pub fn launch_server(server: TcpListener, config_path: PathBuf) -> Result<(), Error> {
    println!("connected on port {}", server.local_addr()?.port());

    let (client, address) = server.accept()?;
    ::colors7::info!("accepted connection at address {}", address);

    let mut board =
        Board::new_from_config(Config::new(&::std::fs::read_to_string(config_path)?)?, None);

    let board_size = board.config.board_size;
    let mut client = Communication {
        stream: client,
        buffer: vec![0; board_size as usize * board_size as usize + 20],
    };

    // Initial 'handshake'
    client.send_and_wait_ok(MessageKind::HelloFromServer, &[])?;
    client.receive_and_send_ok(MessageKind::HelloFromClient, 0)?;
    client.send_and_wait_ok(MessageKind::NbColors, &[board.config.nb_colors])?;
    client.send_and_wait_ok(
        MessageKind::BoardSize,
        &[board_size.to_be_bytes()[0], board_size.to_be_bytes()[1]],
    )?;
    client.send_and_wait_ok(MessageKind::BoardInitialState, &board.board)?;
    client.send_and_wait_ok(MessageKind::YouArePlayerNo, &[2])?;

    println!("{board}");
    // main loop
    loop {
        let letter1 = choose_move::choose_move(&board, PlayerId::Player1);
        println!("{board}");
        client.send_and_wait_ok(MessageKind::PlayMove, &[letter1])?;
        let letter2 = client.receive_and_send_ok(MessageKind::PlayMove, 1)?[0];

        match board.make_move(PlayerId::Player1, letter1)? {
            GameState::Continue => {}
            GameState::Player1Win => {
                client.send_and_wait_ok(MessageKind::GameFinished, &[1])?;
                break;
            }
            GameState::Player2Win => {
                client.send_and_wait_ok(MessageKind::GameFinished, &[2])?;
                break;
            }
            GameState::Tie => {
                client.send_and_wait_ok(MessageKind::GameFinished, &[0])?;
                break;
            }
        }

        match board.make_move(PlayerId::Player2, letter2) {
            Ok(GameState::Continue) => {}
            Ok(GameState::Player1Win) => {
                client.send_and_wait_ok(MessageKind::GameFinished, &[1])?;
                break;
            }
            Ok(GameState::Player2Win) => {
                client.send_and_wait_ok(MessageKind::GameFinished, &[2])?;
                break;
            }
            Ok(GameState::Tie) => {
                client.send_and_wait_ok(MessageKind::GameFinished, &[0])?;
                break;
            }
            Err(err) => return Err(err.into()),
        }
    }

    Ok(())
}

/// Parse the program arguments.
///
/// This is all the 'useful' program arguments (aka no program name as the first argument).
fn parse_args(args: Vec<OsString>) -> Result<(PathBuf, u16), String> {
    let mut config_path = PathBuf::from("7colors.conf");
    let mut port = 0u16;
    let mut prev_arg = ::std::ffi::OsStr::new("");
    for arg in &args {
        if arg == "--help" {
            println!("Usage: client [OPTIONS]");
            println!();
            println!("Options:");
            println!("      --port   port on which to bind the server (0 means the kernel chooses a port)");
            println!("      --config Path of the configuration file");
            println!("      --help   Print this help message");
            ::std::process::exit(0);
        }
        if prev_arg.is_empty() {
            prev_arg = arg.as_os_str();
        } else {
            match prev_arg.to_str() {
                Some("--config") => config_path = PathBuf::from(arg),
                Some("--port") => {
                    port = arg
                        .to_str()
                        .and_then(|a| a.parse().ok())
                        .ok_or_else(|| format!("incorrect port: {arg:?}"))?
                }
                Some(s) => return Err(format!("unrecognized argument: {s}")),
                _ => return Err(format!("unrecognized argument: {prev_arg:?}")),
            }
        }
    }
    Ok((config_path, port))
}
