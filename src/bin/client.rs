#[path = "bin_commons/choose_move.rs"]
mod choose_move;
#[path = "bin_commons/communication.rs"]
mod communication;
#[path = "bin_commons/error.rs"]
mod error;

use self::{
    communication::{Communication, CommunicationError, MessageKind},
    error::Error,
};
use ::colors7::{
    board::{Board, PlayerId},
    config::Config,
    PlayerKind,
};
use ::std::net::{IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr, TcpStream};

#[allow(dead_code)]
fn main() {
    ::colors7::log::init_env_logger();
    if let Err(err) = real_main(::std::env::args().skip(1).collect()) {
        ::colors7::error!("{}", err.0);
    }
}

pub fn real_main(args: Vec<String>) -> Result<(), Error> {
    let address = parse_args(args)?;
    let client = TcpStream::connect(address)?;
    ::colors7::info!("connected to address {}", address);

    let mut client = Communication {
        stream: client,
        buffer: vec![0; 10],
    };

    // Initial 'handshake'
    client.receive_and_send_ok(MessageKind::HelloFromServer, 0)?;
    client.send_and_wait_ok(MessageKind::HelloFromClient, &[])?;

    let nb_colors = client.receive_and_send_ok(MessageKind::NbColors, 1)?[0];
    let board_size = client.receive_and_send_ok(MessageKind::BoardSize, 2)?;
    let board_size = match board_size {
        [x, y] => u16::from_be_bytes([*x, *y]),
        _ => unreachable!(),
    };
    client.buffer = vec![0; 20 + board_size as usize * board_size as usize];
    let board_data = client
        .receive_and_send_ok(
            MessageKind::BoardInitialState,
            board_size as usize * board_size as usize,
        )?
        .to_owned();

    // TODO: change the logic depending on this.
    let _player_no = client.receive_and_send_ok(MessageKind::YouArePlayerNo, 1)?[0];
    let mut board = {
        let mut b = Board::new_from_config(Config::default(), None);
        b.board = board_data;
        b.config.nb_colors = nb_colors;
        b.config.board_size = board_size;
        // hardcoded for now.
        b.config.player2_ia = PlayerKind::Ia(::colors7::IaKind::Glutton);
        b
    };

    println!("{board}");
    let game_finished = loop {
        let letter1 = match client.receive_and_send_ok(MessageKind::PlayMove, 1) {
            Ok(msg) => msg[0],
            Err(CommunicationError::WrongKind { expected: _, got })
                if got == MessageKind::GameFinished as u8 =>
            {
                break client.receive_raw(1)?[0];
            }
            Err(err) => return Err(err.into()),
        };
        board.make_move(PlayerId::Player1, letter1)?;
        println!("{board}");

        let letter2 = self::choose_move::choose_move(&board, PlayerId::Player2);
        client.send_and_wait_ok(MessageKind::PlayMove, &[letter2])?;
        board.make_move(PlayerId::Player2, letter2)?;
        println!("{board}");
    };

    println!("finished {game_finished}");

    Ok(())
}

/// Parse the program arguments.
///
/// This is all the 'useful' program arguments (aka no program name as the first argument).
fn parse_args(args: Vec<String>) -> Result<SocketAddr, String> {
    let mut ip = IpAddr::V4(Ipv4Addr::LOCALHOST);
    let mut port = 8080;
    let mut prev_arg = "";
    for arg in &args {
        if arg == "--help" {
            println!("Usage: client [OPTIONS]");
            println!();
            println!("Options:");
            println!("      --ip     IP address to connect to (IPV4)");
            println!("      --ipv6   IP address to connect to (IPV6)");
            println!("      --port   Port to connect to");
            println!("      --help   Print this help message");
            ::std::process::exit(0);
        }
        if prev_arg.is_empty() {
            prev_arg = arg.as_str();
        } else {
            match prev_arg {
                "--ip" => ip = IpAddr::V4(arg.parse::<Ipv4Addr>().map_err(|e| e.to_string())?),
                "--ipv6" => ip = IpAddr::V6(arg.parse::<Ipv6Addr>().map_err(|e| e.to_string())?),
                "--port" => port = arg.parse::<u16>().map_err(|e| e.to_string())?,
                _ => return Err(format!("unrecognized argument: {prev_arg}")),
            }
            prev_arg = "";
        }
    }
    Ok(SocketAddr::new(ip, port))
}
