#[path = "bin_commons/choose_move.rs"]
mod choose_move;

use self::choose_move::choose_move;
use ::colors7::{
    board::{Board, GameState, PlayerId},
    config::Config,
    Color,
};

fn main() {
    let config = Config::new(&::std::fs::read_to_string("7colors.conf").unwrap()).unwrap();
    let mut board = Board::new_from_config(config, None);

    if let Some(rounds) = config.tournament_rounds {
        let mut p1_wins = 0usize;
        let mut p2_wins = 0usize;
        let mut ties = 0usize;
        for round in 0..rounds {
            print!("\x1b[1F\x1b[K");
            println!("round {}/{rounds}", round + 1);
            match make_game(false, &mut board) {
                GameState::Continue => unreachable!(),
                GameState::Player1Win => p1_wins += 1,
                GameState::Player2Win => p2_wins += 1,
                GameState::Tie => ties += 1,
            }
        }
        println!();
        println!(
            "{}P1 won {p1_wins} games{}",
            board.config.player1_color.color_code_foreground(),
            Color::color_code_reset()
        );
        println!(
            "{}P2 won {p2_wins} games{}",
            board.config.player2_color.color_code_foreground(),
            Color::color_code_reset()
        );
        if ties > 0 {
            println!("{ties} tie{}", if ties == 1 { "" } else { "s" });
        }
    } else {
        make_game(true, &mut board);
    }
}

fn make_game(display: bool, board: &mut Board) -> GameState {
    board.reset();

    if display {
        println!("{board}");
    }

    let final_state = 'mainloop: loop {
        for player in [PlayerId::Player1, PlayerId::Player2] {
            let letter_chosen = choose_move(board, player);
            if display {
                println!("{board}");
            }
            match board.make_move(player, letter_chosen).unwrap() {
                GameState::Continue => {}
                GameState::Player1Win => {
                    if display {
                        println!("player 1 wins!");
                    }
                    break 'mainloop GameState::Player1Win;
                }
                GameState::Player2Win => {
                    if display {
                        println!("player 2 wins!");
                    }
                    break 'mainloop GameState::Player2Win;
                }
                GameState::Tie => {
                    if display {
                        println!("Its a tie.");
                    }
                    break 'mainloop GameState::Tie;
                }
            }
        }
    };
    if display {
        println!(
            "scores: P1: {}, P2: {}",
            board.player_score(PlayerId::Player1),
            board.player_score(PlayerId::Player2)
        );
    }
    final_state
}
