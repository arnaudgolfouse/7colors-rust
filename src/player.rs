use crate::board::{Board, PlayerId};

pub(crate) const PLAYER_1_SYMBOL: u8 = 1;
pub(crate) const PLAYER_2_SYMBOL: u8 = 2;

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum PlayerKind {
    Human,
    Ia(IaKind),
}

impl PlayerKind {
    #[must_use]
    pub fn is_human(&self) -> bool {
        matches!(self, Self::Human)
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum IaKind {
    /// Pick a random color among `b'A'..b'A' + nb_colors`.
    Uniform,
    /// Pick a random color, weighted by the number of each color in the neighboring
    /// cells.
    UniformAmongNeighbors,
    /// Pick the color that makes for the best gain this turn.
    Glutton,
    /// Pick the color that makes for the best gain for this turn and the next turn.
    WiseGlutton,
}

impl Board {
    pub(crate) fn ia_move_uniform(&self) -> u8 {
        self.rng.rand_uniform(b'A', b'A' + self.config.nb_colors)
    }

    pub(crate) fn ia_move_uniform_among_neighbors(&self, player: PlayerId) -> u8 {
        let player_symbol = if player == PlayerId::Player1 {
            PLAYER_1_SYMBOL
        } else {
            PLAYER_2_SYMBOL
        };
        let mut neighbors = vec![0u32; self.config.nb_colors as _];
        'outer: for (tile_index, &tile) in self.board.iter().enumerate() {
            for neighbor in self.tile_neighbors(tile_index as u32).into_iter().flatten() {
                if tile >= b'A' {
                    let neighbor = self.board[neighbor as usize];
                    if neighbor == player_symbol {
                        neighbors[(tile - b'A') as usize] += 1;
                        continue 'outer;
                    }
                }
            }
        }
        let sum_neighbors = neighbors.iter().copied().sum::<u32>();
        let random = if sum_neighbors == 0 {
            0
        } else {
            self.rng.rand_uniform(0, sum_neighbors)
        };
        let mut sum = 0;
        let mut result = 0u8;
        for (index, n) in neighbors.into_iter().enumerate() {
            sum += n;
            if random < sum {
                result = index as u8;
                break;
            }
        }
        result + b'A'
    }

    pub(crate) fn ia_move_glutton(&self, player: PlayerId) -> u8 {
        let mut best_letter = b'A';
        let mut best_score = 0;
        for letter in b'A'..b'A' + self.config.nb_colors {
            let mut clone = self.clone();
            let _: Result<_, _> = clone.make_move(player, letter); // error is impossible, we can ignore it
            let score = if player == PlayerId::Player1 {
                clone.player1_score
            } else {
                clone.player2_score
            };
            if score > best_score {
                best_letter = letter;
                best_score = score;
            }
        }
        best_letter
    }

    pub(crate) fn ia_move_wise_glutton(&self, player: PlayerId) -> u8 {
        let mut best_letter = b'A';
        let mut best_score = 0;
        let mut clone1 = self.clone();
        let mut clone2 = self.clone();
        for letter1 in b'A'..b'A' + self.config.nb_colors {
            clone1.clone_from(self);
            let _: Result<_, _> = clone1.make_move(player, letter1); // errors are impossible, we can ignore them
            let score1 = if player == PlayerId::Player1 {
                clone1.player1_score
            } else {
                clone1.player2_score
            };
            for letter2 in b'A'..b'A' + self.config.nb_colors {
                if letter1 == letter2 {
                    continue;
                }
                clone2.clone_from(&clone1);
                let _: Result<_, _> = clone2.make_move(player, letter2); // errors are impossible, we can ignore them

                let score = if player == PlayerId::Player1 {
                    clone2.player1_score
                } else {
                    clone2.player2_score
                };
                if score > best_score || score == best_score && score == score1 {
                    best_letter = letter1;
                    best_score = score;
                }
            }
        }
        best_letter
    }
}
