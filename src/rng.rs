use ::core::{
    cell::Cell,
    ops::{Add, Sub},
};

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub(crate) struct Rng {
    inner: Cell<u64>,
}

impl Rng {
    /// Initialize the RNG with the global timer
    pub(crate) fn new(seed: Option<u64>) -> Self {
        Self {
            inner: Cell::new(seed.unwrap_or_else(|| {
                u64::from_ne_bytes(
                    ::std::time::SystemTime::UNIX_EPOCH
                        .elapsed()
                        .unwrap()
                        .as_nanos()
                        .to_ne_bytes()[0..8]
                        .try_into()
                        .unwrap(),
                )
            })),
        }
    }

    /// pick a random number uniformely in [from, to[.
    pub(crate) fn rand_uniform<N>(&self, from: N, to: N) -> N
    where
        u64: TryInto<N>,
        N: Copy + Default + Add<Output = N> + Sub<Output = N> + Into<u64>,
    {
        // 187973 and 163847 are primes
        let value = self.inner.get().wrapping_mul(187_973).wrapping_add(163_847);
        let range: u64 = (to - from).into();
        let base: N = (value % range).try_into().unwrap_or_default();
        self.inner.set(value);
        base + from
    }
}
