use ::core::fmt;
use ::std::borrow::Cow;

#[derive(Clone, Debug, Copy, PartialEq)]
pub enum Color {
    Default,
    Black,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    White,
    Rgb { r: u8, g: u8, b: u8 },
}

impl Default for Color {
    fn default() -> Self {
        Self::Default
    }
}

impl Color {
    #[must_use]
    pub fn color_code_foreground(&self) -> Cow<str> {
        match self {
            Self::Default => "".into(),
            Self::Black => "\x1b[30m".into(),
            Self::Red => "\x1b[31m".into(),
            Self::Green => "\x1b[32m".into(),
            Self::Yellow => "\x1b[33m".into(),
            Self::Blue => "\x1b[34m".into(),
            Self::Magenta => "\x1b[35m".into(),
            Self::Cyan => "\x1b[36m".into(),
            Self::White => "\x1b[37m".into(),
            Self::Rgb { r, g, b } => Cow::Owned(format!("\x1b[38;2;{r};{g};{b}m")),
        }
    }

    #[must_use]
    pub fn color_code_background(&self) -> Cow<str> {
        match self {
            Self::Default => "".into(),
            Self::Black => "\x1b[40m".into(),
            Self::Red => "\x1b[41m".into(),
            Self::Green => "\x1b[42m".into(),
            Self::Yellow => "\x1b[43m".into(),
            Self::Blue => "\x1b[44m".into(),
            Self::Magenta => "\x1b[45m".into(),
            Self::Cyan => "\x1b[46m".into(),
            Self::White => "\x1b[47m".into(),
            Self::Rgb { r, g, b } => Cow::Owned(format!("\x1b[48;2;{r};{g};{b}m")),
        }
    }

    #[must_use]
    pub fn color_code_reset() -> &'static str {
        "\x1b[0m"
    }
}

pub struct Colored<T: fmt::Display> {
    color_fg: Color,
    color_bg: Color,
    message: T,
}

impl<T: fmt::Display> fmt::Display for Colored<T> {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        // set foreground color
        if self.color_fg != Color::Default {
            write!(
                formatter,
                "{}",
                self.color_fg.color_code_foreground().as_ref()
            )?;
        }
        // set background color
        if self.color_bg != Color::Default {
            write!(
                formatter,
                "{}",
                self.color_bg.color_code_background().as_ref()
            )?;
        }
        // write message
        write!(formatter, "{}", self.message)?;
        // reset
        formatter.write_str(Color::color_code_reset())
    }
}

impl<T: fmt::Display> Colored<T> {
    pub fn new(message: T, color_foregroung: Color, color_background: Color) -> Self {
        Self {
            color_fg: color_foregroung,
            color_bg: color_background,
            message,
        }
    }
}
