pub mod board;
mod color;
pub mod config;
pub mod log;
mod player;
mod rng;

pub use self::{
    color::Color,
    player::{IaKind, PlayerKind},
};
