use crate::{
    color::Color,
    player::{IaKind, PlayerKind},
};

/// Configuration for the game.
///
/// This is specified by the user with a configuration file, or similar.
#[derive(Clone, Copy, Debug)]
pub struct Config {
    pub player1_ia: PlayerKind,
    pub player2_ia: PlayerKind,
    pub player1_color: Color,
    pub player2_color: Color,
    /// Must be >= 2
    pub board_size: u16,
    /// Must be >= 2
    pub nb_colors: u8,
    pub tournament_rounds: Option<u32>,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            player1_ia: PlayerKind::Human,
            player2_ia: PlayerKind::Ia(IaKind::Glutton),
            player1_color: Color::Red,
            player2_color: Color::Blue,
            board_size: 30,
            nb_colors: 7,
            tournament_rounds: None,
        }
    }
}

#[derive(Clone, Debug)]
pub enum IncorrectConfig {
    NoEqual {
        line: usize,
    },
    MultipleEqual {
        line: usize,
        first: usize,
        second: usize,
    },
    IncorrectValue {
        line: usize,
        value: String,
    },
    IncorrectKey {
        line: usize,
        key: String,
    },
    NotEnoughColors {
        line: usize,
    },
    TooManyColors {
        line: usize,
    },
    BoardTooSmall {
        line: usize,
    },
}

impl ::core::fmt::Display for IncorrectConfig {
    fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
        write!(f, "Incorrect configuration at line {}: ", self.line())?;
        match self {
            IncorrectConfig::NoEqual { .. } => write!(f, "missing '='"),
            IncorrectConfig::MultipleEqual { .. } => write!(f, "multiple '='"),
            IncorrectConfig::IncorrectKey { key, .. } => {
                write!(f, "unknown key {key}")
            }
            IncorrectConfig::IncorrectValue { value, .. } => {
                write!(f, "incorrect value {value}")
            }
            IncorrectConfig::NotEnoughColors { .. } => {
                write!(f, "nb_colors is too low: should be at least 2")
            }
            IncorrectConfig::TooManyColors { .. } => {
                write!(f, "nb_colors is too high: should be at most 26")
            }
            IncorrectConfig::BoardTooSmall { .. } => {
                write!(f, "board_size is too small: should be at least")
            }
        }?;
        Ok(())
    }
}

impl Config {
    /// Parses `configuration_string` to create a new config.
    ///
    /// # Configuration format
    ///
    #[doc = include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/configuration format.md"))]
    /// # Errors
    /// Returns `IncorrectConfig` if the configuration string is incorrect.
    pub fn new(configuration_string: &str) -> Result<Self, IncorrectConfig> {
        // default values
        let mut config = Config {
            player1_ia: PlayerKind::Human,
            player2_ia: PlayerKind::Ia(IaKind::Glutton),
            player1_color: Color::Red,
            player2_color: Color::Blue,
            board_size: 30,
            nb_colors: 7,
            tournament_rounds: None,
        };
        for (line_number, mut line) in configuration_string.lines().enumerate() {
            if let Some((before, _)) = line.split_once('#') {
                line = before;
            }
            line = line.trim();
            if line.is_empty() {
                continue;
            }
            let (key, value) = match parse_assign(line) {
                Ok((k, v)) => (k, v),
                Err(IncorrectConfig::NoEqual { line: _ }) => {
                    return Err(IncorrectConfig::NoEqual { line: line_number })
                }
                Err(IncorrectConfig::MultipleEqual {
                    line: _,
                    first,
                    second,
                }) => {
                    return Err(IncorrectConfig::MultipleEqual {
                        line: line_number,
                        first,
                        second,
                    })
                }
                _ => unreachable!(),
            };
            match key {
                "P1_IA" => config.player1_ia = parse_ia(value, line_number)?,
                "P2_IA" => config.player2_ia = parse_ia(value, line_number)?,
                "P1_color" => config.player1_color = parse_color(value, line_number)?,
                "P2_color" => config.player2_color = parse_color(value, line_number)?,
                "board_size" => {
                    config.board_size =
                        value.parse().map_err(|_| IncorrectConfig::IncorrectValue {
                            line: line_number,
                            value: value.to_owned(),
                        })?;
                    if config.board_size < 2 {
                        return Err(IncorrectConfig::BoardTooSmall { line: line_number });
                    }
                }
                "nb_colors" => {
                    config.nb_colors =
                        value.parse().map_err(|_| IncorrectConfig::IncorrectValue {
                            line: line_number,
                            value: value.to_owned(),
                        })?;
                    if config.nb_colors < 2 {
                        return Err(IncorrectConfig::NotEnoughColors { line: line_number });
                    } else if config.nb_colors > 26 {
                        return Err(IncorrectConfig::TooManyColors { line: line_number });
                    }
                }
                "tournament_round" => {
                    config.tournament_rounds =
                        Some(value.parse().map_err(|_| IncorrectConfig::IncorrectValue {
                            line: line_number,
                            value: value.to_owned(),
                        })?);
                }
                _ => {}
            }
        }

        Ok(config)
    }
}

fn parse_assign(line: &str) -> Result<(&str, &str), IncorrectConfig> {
    if let Some((before, after)) = line.split_once('=') {
        if let Some(index) = after.find('=') {
            Err(IncorrectConfig::MultipleEqual {
                line: 0, // placeholder
                first: before.len(),
                second: before.len() + index + 1,
            })
        } else {
            Ok((before.trim(), after.trim()))
        }
    } else {
        Err(IncorrectConfig::NoEqual { line: 0 }) // placeholder
    }
}

fn parse_ia(value: &str, line: usize) -> Result<PlayerKind, IncorrectConfig> {
    match value.parse::<u8>() {
        Ok(0) => Ok(PlayerKind::Human),
        Ok(1) => Ok(PlayerKind::Ia(IaKind::Uniform)),
        Ok(2) => Ok(PlayerKind::Ia(IaKind::UniformAmongNeighbors)),
        Ok(3) => Ok(PlayerKind::Ia(IaKind::Glutton)),
        Ok(4) => Ok(PlayerKind::Ia(IaKind::WiseGlutton)),
        _ => Err(IncorrectConfig::IncorrectValue {
            line,
            value: value.to_owned(),
        }),
    }
}

fn parse_color(value: &str, line: usize) -> Result<Color, IncorrectConfig> {
    match value {
        "default" => Ok(Color::Default),
        "red" => Ok(Color::Red),
        "green" => Ok(Color::Green),
        "blue" => Ok(Color::Blue),
        "white" => Ok(Color::White),
        "black" => Ok(Color::Black),
        "yellow" => Ok(Color::Yellow),
        "magenta" => Ok(Color::Magenta),
        "cyan" => Ok(Color::Cyan),
        _ => {
            let mut rgb = [0, 0, 0];
            if let Some(mut s) = value.strip_prefix("rgb") {
                s = s.trim();
                if let Some(mut s) = s.strip_prefix('(') {
                    s = s.trim();

                    for (i, symbol) in [(0, ','), (1, ','), (2, ')')] {
                        if let Some((before, after)) = s.split_once(symbol) {
                            s = after;
                            rgb[i] = match before.trim().parse::<u8>() {
                                Ok(x) => x,
                                Err(_) => {
                                    return Err(IncorrectConfig::IncorrectValue {
                                        line,
                                        value: value.to_owned(),
                                    })
                                }
                            };
                        } else {
                            return Err(IncorrectConfig::IncorrectValue {
                                line,
                                value: value.to_owned(),
                            });
                        }
                    }
                } else {
                    return Err(IncorrectConfig::IncorrectValue {
                        line,
                        value: value.to_owned(),
                    });
                }
            } else {
                return Err(IncorrectConfig::IncorrectValue {
                    line,
                    value: value.to_owned(),
                });
            }

            Ok(Color::Rgb {
                r: rgb[0],
                g: rgb[1],
                b: rgb[2],
            })
        }
    }
}

impl IncorrectConfig {
    #[must_use]
    pub fn line(&self) -> usize {
        match self {
            IncorrectConfig::NoEqual { line }
            | IncorrectConfig::MultipleEqual { line, .. }
            | IncorrectConfig::IncorrectKey { line, .. }
            | IncorrectConfig::IncorrectValue { line, .. }
            | IncorrectConfig::NotEnoughColors { line }
            | IncorrectConfig::TooManyColors { line }
            | IncorrectConfig::BoardTooSmall { line } => *line,
        }
    }
}
