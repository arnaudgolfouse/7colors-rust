use crate::{
    color::{Color, Colored},
    config::Config,
    player::{IaKind, PlayerKind, PLAYER_1_SYMBOL, PLAYER_2_SYMBOL},
    rng::Rng,
};
use ::core::fmt;

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct InvalidLetter(u8);

impl std::fmt::Display for InvalidLetter {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "the letter '{}' ('{}') is an invalid move!",
            self.0 as char, self.0
        )
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum PlayerId {
    Player1,
    Player2,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum GameState {
    Continue,
    Player1Win,
    Player2Win,
    Tie,
}

#[derive(Clone, Debug)]
pub struct Board {
    pub(crate) rng: Rng,
    pub config: Config,
    pub board: Vec<u8>,
    pub(crate) player1_score: u32,
    pub(crate) player2_score: u32,
    tiles_to_visit_buffer: Vec<u32>,
}

impl Board {
    #[must_use]
    pub fn new_from_config(config: Config, rng_seed: Option<u64>) -> Self {
        let rng = Rng::new(rng_seed);

        let board_size = config.board_size.into();
        let mut board = Vec::with_capacity(board_size * board_size);
        Self::fill_board_random(&mut board, &rng, board_size, config.nb_colors);

        Self {
            rng,
            config,
            tiles_to_visit_buffer: Vec::with_capacity(board.len()),
            board,
            player1_score: 1,
            player2_score: 1,
        }
    }

    pub fn reset(&mut self) {
        let board_size = self.config.board_size as usize;
        Self::fill_board_random(
            &mut self.board,
            &self.rng,
            board_size,
            self.config.nb_colors,
        );
        self.player1_score = 1;
        self.player2_score = 1;
    }

    pub(crate) fn tile_neighbors(&self, tile: u32) -> [Option<u32>; 4] {
        let board_size = u32::from(self.config.board_size);
        [
            // left
            if tile % board_size == 0 {
                None
            } else {
                Some(tile - 1)
            },
            // right
            if (tile + 1) % board_size == 0 {
                None
            } else {
                Some(tile + 1)
            },
            // top
            tile.checked_sub(board_size),
            // bottom
            if tile + board_size >= board_size * board_size {
                None
            } else {
                Some(tile + board_size)
            },
        ]
    }

    /// `letter` should be between `b'A'` and `b'A' + config.nb_colors`.
    ///
    /// # Errors
    /// return `InvalidLetter(letter)` if `letter` is out of the valid letter range
    /// (`b'A'..b'A' + nb_colors`)
    pub fn make_move(&mut self, player: PlayerId, letter: u8) -> Result<GameState, InvalidLetter> {
        let player_symbol = match player {
            PlayerId::Player1 => PLAYER_1_SYMBOL,
            PlayerId::Player2 => PLAYER_2_SYMBOL,
        };
        if letter < b'A' || letter >= b'A' + self.config.nb_colors {
            return Err(InvalidLetter(letter));
        }
        for (index, tile) in self.board.iter().enumerate() {
            if *tile == player_symbol {
                self.tiles_to_visit_buffer.push(index as u32);
            }
        }
        let board_size = u32::from(self.config.board_size);
        while let Some(to_visit) = self.tiles_to_visit_buffer.pop() {
            let neighbors = self.tile_neighbors(to_visit);
            for neighbor in neighbors.into_iter().flatten() {
                if self.board[neighbor as usize] == letter {
                    self.board[neighbor as usize] = player_symbol;
                    self.tiles_to_visit_buffer.push(neighbor);
                    if player == PlayerId::Player1 {
                        self.player1_score += 1;
                    } else {
                        self.player2_score += 1;
                    }
                }
            }
        }
        if self.player1_score > (board_size * board_size) / 2 {
            Ok(GameState::Player1Win)
        } else if self.player2_score > (board_size * board_size) / 2 {
            Ok(GameState::Player2Win)
        } else if self.player1_score + self.player2_score == board_size * board_size {
            Ok(GameState::Tie)
        } else {
            Ok(GameState::Continue)
        }
    }

    pub fn choose_ia_move(&self, player: PlayerId, ia_kind: IaKind) -> u8 {
        match ia_kind {
            IaKind::Uniform => self.ia_move_uniform(),
            IaKind::UniformAmongNeighbors => self.ia_move_uniform_among_neighbors(player),
            IaKind::Glutton => self.ia_move_glutton(player),
            IaKind::WiseGlutton => self.ia_move_wise_glutton(player),
        }
    }

    pub fn player_score(&self, player: PlayerId) -> u32 {
        match player {
            PlayerId::Player1 => self.player1_score,
            PlayerId::Player2 => self.player2_score,
        }
    }

    pub fn player_kind(&self, player: PlayerId) -> PlayerKind {
        match player {
            PlayerId::Player1 => self.config.player1_ia,
            PlayerId::Player2 => self.config.player2_ia,
        }
    }

    pub fn player_color(&self, player: PlayerId) -> Color {
        match player {
            PlayerId::Player1 => self.config.player1_color,
            PlayerId::Player2 => self.config.player2_color,
        }
    }

    fn fill_board_random(board: &mut Vec<u8>, rng: &Rng, board_size: usize, nb_colors: u8) {
        board.clear();
        for _ in 0..board_size * board_size {
            board.push(rng.rand_uniform(b'A', b'A' + nb_colors));
        }
        board[0] = PLAYER_1_SYMBOL;
        board[board_size * board_size - 1] = PLAYER_2_SYMBOL;
    }

    fn get_color(letter: u8) -> Color {
        const COLORS: &[Color] = &[
            Color::Red,
            Color::Yellow,
            Color::Green,
            Color::Cyan,
            Color::Blue,
            Color::Magenta,
            Color::White,
        ];
        let index = letter as usize % COLORS.len();
        COLORS[index]
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for (index, &tile) in self.board.iter().enumerate() {
            if tile == PLAYER_1_SYMBOL {
                write!(
                    f,
                    "{}",
                    Colored::new(" ", Color::default(), self.config.player1_color)
                )
            } else if tile == PLAYER_2_SYMBOL {
                write!(
                    f,
                    "{}",
                    Colored::new(" ", Color::default(), self.config.player2_color)
                )
            } else {
                write!(
                    f,
                    "{}",
                    Colored::new(tile as char, Self::get_color(tile), Color::default())
                )
            }?;
            if (index + 1) % self.config.board_size as usize == 0 {
                f.write_str("\n")?;
            }
        }
        f.write_str("\n")?;
        writeln!(
            f,
            "{}Player 1 score: {}{}",
            self.config.player1_color.color_code_foreground(),
            self.player1_score,
            Color::color_code_reset()
        )?;
        writeln!(
            f,
            "{}Player 2 score: {}{}",
            self.config.player2_color.color_code_foreground(),
            self.player2_score,
            Color::color_code_reset()
        )?;
        Ok(())
    }
}
