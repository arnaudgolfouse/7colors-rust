use ::core::sync::atomic::AtomicUsize;

pub use crate::{error, info, trace, warn};

#[doc(hidden)]
pub static LOG_LEVEL: AtomicUsize = AtomicUsize::new(0);

/// Initialize logging in the crate, based on the value of the `LOG_LEVEL` env
/// variable.
///
/// Defaults to `0`, aka only errors are shown.
pub fn init_env_logger() {
    let level = if let Ok(level) = ::std::env::var("LOG_LEVEL") {
        match level.as_str() {
            "WARN" | "warn" => 1,
            "INFO" | "info" => 2,
            "TRACE" | "trace" => 3,
            _ => 0,
        }
    } else {
        0
    };
    LOG_LEVEL.store(level, ::core::sync::atomic::Ordering::SeqCst);
}

/// Print an error message.
#[macro_export]
macro_rules! error {
    ($format_str:literal $(, $($args:expr),+)?) => {
        ::std::println!(concat!("{}[ERROR]{} ", $format_str), $crate::Color::Red.color_code_foreground(), $crate::Color::color_code_reset(), $($($args),+)?);
    };
}

/// Print a warning message if `LOG_LEVEL >= 1`.
#[macro_export]
macro_rules! warn {
    ($format_str:literal $(, $($args:expr),+)?) => {
        if $crate::log::LOG_LEVEL.load(::core::sync::atomic::Ordering::SeqCst) >= 1 {
            ::std::println!(concat!("{}[WARN]{} ", $format_str), $crate::Color::Yellow.color_code_foreground(), $crate::Color::color_code_reset(), $($($args),+)?);
        }
    };
}

/// Print an informative message if `LOG_LEVEL >= 2`.
#[macro_export]
macro_rules! info {
    ($format_str:literal $(, $($args:expr),+)?) => {
        if $crate::log::LOG_LEVEL.load(::core::sync::atomic::Ordering::SeqCst) >= 2 {
            ::std::println!(concat!("{}[INFO]{} ", $format_str), $crate::Color::Blue.color_code_foreground(), $crate::Color::color_code_reset(), $($($args),+)?);
        }
    };
}

/// Print a tracing message if `LOG_LEVEL >= 3`.
#[macro_export]
macro_rules! trace {
    ($format_str:literal $(, $($args:expr),+)?) => {
        if $crate::log::LOG_LEVEL.load(::core::sync::atomic::Ordering::SeqCst) >= 3 {
            ::std::println!(concat!("[TRACE] ", $format_str), $($($args),+)?);
        }
    };
}
