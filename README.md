# 7colors

This is a school project, initially in C, now rewritten in Rust with a client/server implementation.

## Usage

Use:

- `cargo run` to run a local game based on the [configuration file](./configuration%20format.md).
- `cargo run --bin server` to start a game server.
- `cargo run --bin client` to try and connect to an existing server. Command usage can be seen with `cargo run --bin client -- --help`.
