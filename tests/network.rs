#![allow(clippy::duplicate_mod)]

#[path = "../src/bin/client.rs"]
mod client;
#[path = "../src/bin/server.rs"]
mod server;

use std::{path::Path, thread};

#[test]
fn network() {
    let configs_dir = Path::new("tests/configs/");
    thread::scope(|s| {
        for config in configs_dir.read_dir().unwrap() {
            let config = config.unwrap().path();
            let (server, config_path) =
                match server::open_connection(vec!["--config".into(), config.as_path().into()]) {
                    Ok(res) => res,
                    Err(err) => {
                        panic!("with config {}: {}", config.display(), err.0)
                    }
                };
            let port = server.local_addr().unwrap().port().to_string();
            let config_clone = config.clone();

            s.spawn(move || {
                if let Err(e) = server::launch_server(server, config_path) {
                    panic!("with config {}: {}", config_clone.display(), e.0)
                }
            });
            s.spawn(move || {
                if let Err(e) = client::real_main(vec![
                    "--ip".into(),
                    "127.0.0.1".into(),
                    "--port".into(),
                    port,
                ]) {
                    panic!("with config {}: {}", config.display(), e.0)
                }
            });
        }
    });
}
